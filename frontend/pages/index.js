import styled from "@emotion/styled";

export default function index() {
  return <Title>window shopper</Title>;
}

const Title = styled.div`
  padding: 32px;
  cursor: pointer;
  background-color: hotpink;
  font-size: 24px;
  color: black;
  font-weight: bold;
  &:hover {
    color: red;
  }
`;
