import Listing from "../../components/ListingPage/Listing";
import Navbar from "../../components/Navigation/NavBar";

function listpage() {
  return (
    <>
      <Navbar />
      <Listing />
    </>
  );
}

export default listpage;
