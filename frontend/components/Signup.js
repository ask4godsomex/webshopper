import { useState } from 'react';
import axios from 'axios';

import { Card, Email, Password, Button } from '../styles';

function Signup() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = async (e) => {
    e.preventDefault();

    const res = await axios.post('/api/users/signup', {
      email,
      password,
    });
    console.log(res);
  };
  
  return (
    <Card>
      <form onSubmit={onSubmit}>
        <Email
          value={email}
          placeholder="Enter Email"
          onChange={(e) => setEmail(e.target.value)}
        />
        <Password
          value={password}
          placeholder="Enter Password"
          onChange={(e) => setPassword(e.target.value)}
          type="password"
        />
        <Password
          placeholder="Enter Password Again"
          onChange={(e) => setEmail(e.target.value)}
          type="password"
        />
        <Button>Signup</Button>
      </form>
    </Card>
  );
}

export default Signup;
