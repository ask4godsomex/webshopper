import React, { useRef, useState } from "react";
import Image from "next/image";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/navigation/navigation.min.css";
import "swiper/components/thumbs/thumbs.min.css";

// import Swiper core and required modules
import SwiperCore, { Navigation, Thumbs } from "swiper/core";

// install Swiper modules
SwiperCore.use([Navigation, Thumbs]);

export default function App() {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  return (
    <>
      <Swiper
        style={{
          "--swiper-navigation-color": "#fff",
          "--swiper-pagination-color": "#fff",
        }}
        spaceBetween={10}
        navigation={true}
        thumbs={{ swiper: thumbsSwiper }}
        className="mySwiper2"
      >
        <SwiperSlide>
          <img src="/images/img_1.jpg" alt="phones" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="/images/img_2.jpg" alt="phones" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="/images/img_3.jpg" alt="phones" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="/images/img_4.jpg" alt="phones" />
        </SwiperSlide>
      </Swiper>
      <Swiper
        onSwiper={setThumbsSwiper}
        spaceBetween={4}
        slidesPerView={4}
        freeMode={true}
        watchSlidesVisibility={true}
        watchSlidesProgress={true}
        className="mySwiper"
      >
        <SwiperSlide>
          <img
            src="/images/img_1.jpg"
            className="object-scale-down"
            alt="phones"
            width={50}
            height={50}
          />
        </SwiperSlide>
        <SwiperSlide>
          <img src="/images/img_2.jpg" layout="fill" alt="phones" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="/images/img_3.jpg" layout="fill" alt="phones" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="/images/img_4.jpg" layout="fill" alt="phones" />
        </SwiperSlide>
      </Swiper>
    </>
  );
}
