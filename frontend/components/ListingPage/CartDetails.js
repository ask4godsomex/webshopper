import React from "react";
import styled from "@emotion/styled";

function CartDetails({ cart, setCart }) {
  function removeFromCart(removedProduct) {
    setCart(cart.filter((product) => product !== removedProduct));
  }
  function clearCart() {
    console.log("clear cart");
    setCart([]);
  }
  function totalSum() {
    return cart.reduce((sum, { cost }) => sum + cost, 0);
  }

  return (
    <>
      <h1>Cart {cart.length == 0 && "is empty"} </h1>

      <ProductView>
        {cart.map((product, index) => (
          <div key={index}>
            <img src={product.image} alt={product.name} />
            <h3>{product.name} </h3>
            <h4>{product.cost}€</h4>
            {/* <h4>more details {product.details} </h4> */}
            <span>
              <button onClick={() => removeFromCart(product)}>Remove</button>{" "}
            </span>
          </div>
        ))}
      </ProductView>
      {cart.length > 0 && <h2>total cost: €{totalSum()} </h2>}
      {cart.length > 0 && (
        <button onClick={() => clearCart()}>Clear Cart</button>
      )}
    </>
  );
}

export default CartDetails;

const ProductView = styled.span`
  display: grid;
  grid-template-columns: 1fr 1fr;
  padding: 10px;
`;
