import styled from "@emotion/styled";

function SideBar() {
  return (
    <SideBarSection>
      <h1>Lorem Ipsum </h1>
      <>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
        <li>lorem ipsum</li>
      </>
    </SideBarSection>
  );
}

export default SideBar;

const SideBarSection = styled.span`
  display: none;
  @media screen and (min-width: 800px) {
    display: flex;
    flex-direction: column;
    text-align: center;
    padding: 10px;

    ul,
    li {
      list-style-type: none;
      font-size: 1.5rem;
    }
  }
`;
