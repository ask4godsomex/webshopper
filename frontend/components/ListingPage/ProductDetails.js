import React, { useState } from "react";
import ProductData from "./data.json";
import styled from "@emotion/styled";

function ProductDetails({ setCart, cart }) {
  const [products, setProducts] = useState(ProductData);

  function addToCart(product) {
    setCart([...cart, { ...product }]);
  }

  return (
    <>
      <h1>Products </h1>
      <ProductView>
        {products.map((product, index) => (
          <div key={index}>
            <img src={product.image} alt={product.name} />
            <h3>{product.name} </h3>
            <h4>{product.cost}€ </h4>
            {/* <h4>{product.details} </h4> */}
            <span>
              <button onClick={() => addToCart(product)}>Add</button>{" "}
            </span>
          </div>
        ))}
      </ProductView>
    </>
  );
}

export default ProductDetails;

const ProductView = styled.span`
  display: grid;
  grid-template-columns: 1fr 1fr;
  padding: 10px;

  @media screen and (min-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    padding: 10px;
  }
`;
