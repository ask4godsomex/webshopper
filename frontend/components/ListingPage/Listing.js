import { useState } from "react";
import styled from "@emotion/styled";
import { FaSortAmountDown } from "react-icons/fa";
import ProductDetails from "./ProductDetails";
import CartDetails from "./CartDetails";
import SideBar from "./SideBar";

const Page_Products = "products";
const Page_Cart = "cart";

function Listing() {
  const [cart, setCart] = useState([]);
  const [page, setPage] = useState(Page_Products);

  function navigateTo(nextPage) {
    setPage(nextPage);
  }

  return (
    <ListSection>
      <CartProduct>
        <button onClick={() => navigateTo(Page_Cart)}>
          {cart.length == 1
            ? `${cart.length} item in cart`
            : cart.length > 1
            ? `${cart.length} items in cart`
            : "cart empty"}
        </button>
        <button onClick={() => navigateTo(Page_Products)}>Products</button>
      </CartProduct>
      <FilterCategory>
        <button>Category</button> &nbsp;
        <button>
          &nbsp;
          <FaSortAmountDown /> Filter
        </button>
      </FilterCategory>
      <MainSection>
        <ProductList>
          <SideBar />
        </ProductList>

        <ProductList>
          {page === Page_Products && (
            <ProductDetails cart={cart} setCart={setCart} />
          )}

          {page === Page_Cart && <CartDetails cart={cart} setCart={setCart} />}
        </ProductList>
      </MainSection>
    </ListSection>
  );
}

export default Listing;

const MainSection = styled.div`
  @media screen and (min-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 3fr;
  }
`;

const ListSection = styled.div`
  display: flex;
  flex-direction: column;

  img {
    width: 100%;
    height: 200px;
    object-fit: cover;
  }
`;
const ProductList = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  div {
    border: 2px solid black;
    border-radius: 5px;
    margin: 5px;
    padding: 5px;
    & > span {
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
`;
const FilterCategory = styled.div`
  display: flex;
  justify-content: space-between;
  button {
    width: 100%;
    height: 2.5rem;
    background-color: white;
    border: 1px solid black;
  }
`;
const CartProduct = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  button {
    width: 100%;
    height: 2.5rem;
    background-color: white;
    border: 1px solid black;
  }
`;
