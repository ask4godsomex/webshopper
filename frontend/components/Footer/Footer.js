import styled from "@emotion/styled";
import {
  FaApple,
  FaFacebook,
  FaGooglePlay,
  FaGooglePlusG,
  FaInstagram,
  FaTwitter,
  FaWhatsapp,
} from "react-icons/fa";
import MasterCard from "./my svgs/MasterCard";
import Maestro from "./my svgs/Maestro";
import Cirrus from "./my svgs/Cirrus";
import Paypal from "./my svgs/Paypal";
import Visa from "./my svgs/Visa";

function Footer() {
  return (
    <FooterSection>
      <FooterTop>
        <DropDown>
          <h3>Help & Contact</h3>
          <span>
            {" "}
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
          </span>
        </DropDown>

        <DropDown>
          <h3>Gift Cards</h3>
          <span>
            {" "}
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
          </span>
        </DropDown>
        <DropDown>
          <h3>About us</h3>
          <span>
            {" "}
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
          </span>
        </DropDown>
        <DropDown>
          <h3>Our Outlets</h3>
          <span>
            {" "}
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
          </span>
        </DropDown>
        <DropDown>
          <h3>Our Partners</h3>
          <span>
            {" "}
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
          </span>
        </DropDown>
        <DropDown>
          <h3>We also accept</h3>
          <span>
            {" "}
            <a href="#">
              <MasterCard />{" "}
            </a>{" "}
            <a href="#">
              <Cirrus />{" "}
            </a>
            <a href="#">
              <Maestro />{" "}
            </a>
            <a href="#">
              <Paypal />{" "}
            </a>
            <a href="#">
              <Visa />{" "}
            </a>
            {/* <a href="#">
              <Klarna />{" "}
            </a> */}
          </span>
        </DropDown>
        <DropDown>
          <h3>Our Promises</h3>{" "}
          <span>
            {" "}
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
          </span>
        </DropDown>
        <DropDown>
          <h3>For Partnership</h3>
          <span>
            {" "}
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
            <a href="#">lorem ipsum</a>
          </span>
        </DropDown>
      </FooterTop>
      <FooterBottom>
        <ul>
          <ListItems>
            <a href="#">Imprint</a>
            <a href="#">Terms & Conditions</a>
            <a href="#">Privacy Notice</a>
            <a href="#">Data preferences</a>
          </ListItems>
          <p>
            All prices include VAT The crossed out price indicates the
            manufacturer’s recommended retail price
          </p>
        </ul>

        <ul>
          {" "}
          <h3>webshopper apps </h3>
          <ButtonSection>
            <a href="#">
              <Button1>
                <i>
                  <FaApple />
                </i>
                <p>
                  Download on the <br /> App Store{" "}
                </p>
              </Button1>
            </a>
            <a href="#">
              {" "}
              <Button2>
                <i>
                  <FaGooglePlay />
                </i>
                <p>
                  Available on <br /> Google Play{" "}
                </p>
              </Button2>
            </a>
          </ButtonSection>
        </ul>

        <ul>
          <h3> you can check us out on </h3>
          <SocialIcon>
            <li>
              <a href="#">
                <i>
                  {" "}
                  <FaFacebook />
                </i>
              </a>
            </li>
            <li>
              <a href="#">
                <i>
                  <FaInstagram />
                </i>
              </a>
            </li>
            <li>
              <a href="#">
                <i>
                  <FaTwitter />
                </i>
              </a>
            </li>
            <li>
              <a href="#">
                <i>
                  <FaWhatsapp />
                </i>
              </a>
            </li>
            <li>
              <a href="#">
                <i>
                  <FaGooglePlusG />
                </i>
              </a>
            </li>
          </SocialIcon>
        </ul>
      </FooterBottom>
    </FooterSection>
  );
}

export default Footer;

const FooterSection = styled.div`
  display: flex;
  flex-direction: column;
  position: fixed;
  left: 0;
  bottom: 0;
  color: #000000;
  width: 100%;

  a {
    color: #000000;
    text-decoration: none;
  }
`;
const FooterTop = styled.div`
  background-color: #000;
  color: #fff;
  display: flex;
  flex-direction: column;

  @media screen and (min-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
  }
`;

const DropDown = styled.div`
  text-align: center;
  a {
    text-decoration: none;
    display: block;
    color: #fff;
  }

  h3 {
    margin-bottom: 0;
  }

  :hover span {
    display: block;
    transition: 0.2s;
  }
  span {
    display: none;
    margin-top: 0;
  }

  @media screen and (min-width: 800px) {
    span {
      display: flex;
      flex-direction: column;
      align-text: center;
    }
    :nth-child(6) span {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
    }
  }
`;

const FooterBottom = styled.div`
  display: flex;
  background-color: #d9d9d9;
  flex-direction: column;

  ul {
    flex-grow: 1;
    text-align: center;
  }
  @media screen and (min-width: 800px) {
    flex-direction: row;
  }
`;
const ListItems = styled.div`
  display: flex;
  justify-content: space-between;
`;
const ButtonSection = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

const Button1 = styled.div`
  display: block;
  width: 120px;
  height: 60px;
  border: 1px solid black;
  overflow: hidden;
  text-align: center;
  border-radius: 5%;

  i {
    margin: 0;
  }
  p {
    margin: 0;
  }
`;
const Button2 = styled.div`
  display: block;
  width: 120px;
  height: 60px;
  border: 1px solid black;
  overflow: hidden;
  text-align: center;
  border-radius: 5%;
  i {
    margin: 0;
  }
  p {
    margin: 0;
  }
`;

const SocialIcon = styled.div`
  display: flex;
  justify-content: space-around;
  list-style-type: none;
  margin: 0;
  padding: 0;
  li:nth-child(1) a:hover i {
    color: #3b5998;
  }
  li:nth-child(2) a:hover i {
    color: #e4405f;
  }
  li:nth-child(3) a:hover i {
    color: #00aced;
  }
  li:nth-child(4) a:hover i {
    color: #00ff00;
  }
  li:nth-child(5) a:hover i {
    color: #dd4b39;
  }
  li a {
    width: 60px;
    height: 60px;
    display: block;
    text-align: center;
    margin: 0 10px;
    border-radius: 50%;
    padding: 6px;
    box-sizing: border-box;
    text-decoration: none;
    box-shadow: 0 10px 15px rgba(0, 0, 0, 0.7);
    background: linear-gradient(0deg, #ddd, #fff);
    transition: 0.5s;
    :hover {
      box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
      text-decoration: none;
    }

    i {
      width: 100%;
      height: 100%;
      display: block;
      background: linear-gradient(0deg, #fff, #ddd);
      border-radius: 50%;
      line-height: calc(60px - 12px);
      font-size: 24px;
      color: #262626;
      transition: 0.5s;
    }
  }
`;

// const DropDown = styled.div`
//   border: 5px solid #ffff00;
//   text-align: center;
//   h3 {
//     margin-bottom: 0;
//   }

//   :hover ul {
//     display: block;
//   }
//   ul {
//     display: none;
//     margin-top: 0;

//     a {
//       text-decoration: none;
//       display: block;
//     }
//   }

//   @media screen and (min-width: 800px) {
//     ul {
//       display: flex;
//       flex-direction: column;
//       align-text: center;
//     }
//   }
// `;
