import { useState } from "react";
import styled from "@emotion/styled";

function SearchBar() {
  const [search, setSearch] = useState("");

  return (
    <Search>
      <input
        id="search"
        value={search}
        placeholder="SEARCH"
        type="text"
        onChange={(e) => setSearch(e.target.value)}
        autoComplete="off"
      />
    </Search>
  );
}

export default SearchBar;

const Search = styled.form`
  width: auto;

  input {
    height: 23px;
    width: 40vw;
    color: black;
    outline: none;
  }
`;
