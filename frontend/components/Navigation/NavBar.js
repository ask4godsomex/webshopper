import styled from "@emotion/styled";
import { FaBars, FaTimes } from "react-icons/fa";
import { BiCart, BiGlobe, BiUser } from "react-icons/bi";
import { useState } from "react";
import SearchBar from "./SearchBar";

function Navbar() {
  const [showmenu, updateShowmenu] = useState(false);

  return (
    <Header>
      <LeftNav>
        <ShopItems>
          <a>
            <li>women</li>
          </a>
          <a>
            <li>men</li>
          </a>
          <a>
            <li>kids</li>
          </a>
          <a>
            <li>collections</li>
          </a>
        </ShopItems>
        <i>webshopper</i>
        <NavIcons>
          <a>
            <li>
              <BiGlobe />
            </li>
          </a>
          <a>
            <li>
              <BiUser />
            </li>
          </a>

          <a>
            <li>
              <BiCart />
            </li>
          </a>
        </NavIcons>
      </LeftNav>

      <RightNav>
        <button onClick={() => updateShowmenu(!showmenu)}>
          {showmenu ? <FaTimes /> : <FaBars />}
        </button>

        <div>
          <SearchBar />
        </div>
      </RightNav>
      <ShopItems2 showmenu={showmenu}>
        <a>
          <li>women</li>
        </a>
        <a>
          <li>men</li>
        </a>
        <a>
          <li>kids</li>
        </a>
        <a>
          <li>collections</li>
        </a>
      </ShopItems2>
    </Header>
  );
}

export default Navbar;

const Header = styled.div`
  display: flex;
  flex-direction: column;
  text-decoration: none;
  list-style-type: none;
  background: rgb(69, 69, 185);
  color: #ffffff;
  font-weight: bold;
  width: 100%;
  margin: 0;
  padding: 0;

  i {
    font-size: 1rem;
    letter-spacing: 0.1rem;
    cursor: pointer;
    padding: 0.5rem 1rem;
    text-transform: uppercase;
    :hover {
      color: goldenrod;
    }
  }
`;

const LeftNav = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 2px 5px;
`;
const NavIcons = styled.div`
  display: flex;
  justify-content: space-around;
  a {
    font-size: 1rem;
    letter-spacing: 0.1rem;
    cursor: pointer;
    padding: 0.5rem 1rem;
    color: white;
    text-decoration: none;
  }
`;
const ShopItems = styled.div`
  display: none;
  text-transform: uppercase;
  a {
    font-size: 1rem;
    letter-spacing: 0.1rem;
    cursor: pointer;
    padding: 0.5rem 1rem;
    color: white;
    text-decoration: none;
    :hover,
    :active {
      padding-left: 0.5rem;
      color: goldenrod;
    }
  }

  @media screen and (min-width: 800px) {
    display: flex;
  }
`;
const ShopItems2 = styled.div`
  overflow: hidden;
  list-style-type: none;
  height: ${({ showmenu }) => (showmenu ? "auto" : "0")};
  text-transform: uppercase;
  a {
    font-size: 1rem;
    letter-spacing: 0.1rem;
    display: block;
    padding: 0.5rem 1rem;
    color: white;
    text-decoration: none;
    cursor: pointer;
    :hover {
      padding-left: 1.5rem;
      // border-bottom: 3px solid white;
      color: goldenrod;
    }
  }

  @media screen and (min-width: 800px) {
    display: none;
  }
`;

const RightNav = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 2px 5px;
  border-top: 1px white solid;

  button {
    font-size: 1.5rem;
    color: #ffffff;
    background: transparent;
    border-color: transparent;
    cursor: pointer;
    outline: none;
    padding: 0 0.5rem;
  }

  @media screen and (min-width: 800px) {
    justify-content: flex-end;
    button {
      display: none;
    }
  }
`;
