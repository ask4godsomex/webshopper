import { useState } from "react";
import styled from "@emotion/styled";
import { Card, Email, Password, Button } from "../styles";

function Signin() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const Submit = (e) => {
    e.preventDefault();
    console.log(email, password);
  };

  return (
    <form onSubmit={Submit}>
      <Card>
        <Email
          value={email}
          placeholder="Enter your email here"
          onChange={(e) => setEmail(e.target.value)}
        />
        <Password
          value={password}
          type="password"
          placeholder="Enter your password here"
          onChange={(e) => setPassword(e.target.value)}
        />
        <HoveredButton> Sign in </HoveredButton>
      </Card>
    </form>
  );
}

// Just a test
const HoveredButton = styled(Button)`
  &:hover {
    cursor: pointer;
  }
`;

export default Signin;
