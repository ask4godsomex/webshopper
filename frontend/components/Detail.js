import React, { useState, useEffect } from "react";
import { FaBars, FaShoppingCart, FaBackspace } from "react-icons/fa";

import Picture from "./Picture";

import Navbar from "./Navigation/NavBar";
import { AiOutlineDown, AiOutlineUp } from "react-icons/ai";

export default function Detail() {
  const [data, setData] = useState([]);
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div>
      <Navbar />
      <main className=" min-h-screen md:flex md:justify-between lg:max-w-6xl mx-auto p-4 rounded-sm">
        <div className="image-container md:h-2/4 md:w-1/2  bg-gray-100 my-4  md:p-4 ">
          <h2 className="text-xl m-4 ">Apple iPhone 11, 128GB, Black </h2>
          <Picture />
        </div>

        <div className=" w-full mt-16 md:mt-0 md:px-4 ">
          <div className="max-w-xs mx-auto py-4">
            <div>
              <p className="text-2xl leading-8">Prices</p>
              <ul className="flex">
                <li className="pr-2 text-xl ">849.99 USD</li>
                <li className="">VAT included</li>
              </ul>
            </div>

            <div className="mt-8 ">
              <button className="w-full p-4 my-2 bg-black rounded-sm text-white font-bold ">
                Add to Basket
              </button>
              <button className="w-full p-4 bg-black rounded-sm text-white font-bold ">
                Buy Now
              </button>
            </div>

            <div className="card-info border-gray-400 border mt-12">
              <ul className="">
                <li className="border-b border-gray-400   p-4 text-center">
                  shipped by webshopper
                </li>
                <li className="border-b border-gray-400 p-4">
                  <p>fast delivery</p>
                  <p>1-3 working day</p>
                  <ul className="flex justify-between">
                    <li>Fast delivery</li>
                    <li>10 EUR</li>
                  </ul>
                </li>

                <li className="border-b border-gray-400 p-4">
                  <p>deliver</p>
                  3-5 working days
                  <ul className="flex justify-between">
                    <li>Standard delivery</li>
                    <li>Free</li>
                  </ul>
                </li>

                <li className="border-b border-gray-400 p-4">
                  Free Delivery and free returns
                </li>

                <li className=" p-4">
                  <p>30 days return policy</p>
                </li>
              </ul>
            </div>
          </div>

          <div onClick={() => setIsOpen(!isOpen)}>
            <div className="accordion flex justify-between w-full border-t bg-white active:bg-gray-400 hover:bg-gray-400 text-black font-semibold text-left text-lg cursor-pointer p-4  transition duration-150">
              <span>Description</span>
              <span>{!isOpen ? <AiOutlineDown /> : <AiOutlineUp />}</span>
            </div>
            {isOpen && (
              <div class="panel px-4 bg-white overflow-hidden">
                <p>
                  "Color:Black | Size:128GB | Style:Fully Unlocked Just the
                  right amount of everything. A new dual‑camera system captures
                  more of what you see and love. The fastest chip ever in a
                  smartphone and all‑day battery life let you do more and charge
                  less. And the highest‑quality video in a smartphone, so your
                  memories look better than ever."
                </p>
              </div>
            )}
          </div>

          <div onClick={() => setIsOpen(!isOpen)}>
            <div className="accordion flex justify-between w-full border-t bg-white active:bg-gray-400 hover:bg-gray-400 text-black font-semibold text-left text-lg cursor-pointer p-4  transition duration-150">
              <span>Details and features</span>{" "}
              <span>{!isOpen ? <AiOutlineDown /> : <AiOutlineUp />}</span>
            </div>
            {isOpen && (
              <div class="panel px-4 bg-white overflow-hidden">
                <p>
                  "Fully unlocked and compatible with any carrier of choice
                  (e.g. AT&T, T-Mobile, Sprint, Verizon, US-Cellular, Cricket,
                  Metro, etc.)." 1:"The device does not come with headphones or
                  a SIM card. It does include a charger and charging cable that
                  may be generic, in which case it will be UL or Mfi (Made for
                  iPhone) Certified." 2:"Inspected and guaranteed to have
                  minimal cosmetic damage, which is not noticeable when the
                  device is held at arms length." 3:"Successfully passed a full
                  diagnostic test which ensures like-new functionality and
                  removal of any prior-user personal information." 4:"Tested for
                  battery health and guaranteed to have a minimum battery
                  capacity of 80%.
                </p>
              </div>
            )}
          </div>
        </div>
      </main>
    </div>
  );
}
